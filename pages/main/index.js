// pages/main/index.js
var gbData = getApp().globalData

Page({

  /**
   * 页面的初始数据
   */
  data: {
    text: "This is page data.",
    imgUrls: [
      'poster1.jpg',
      'poster2.jpg',
      'poster3.jpg',
      'poster4.jpg',
      'poster5.jpg'
    ],
    indicatorDots: true,
    autoplay: true,
    interval: 5000,
    duration: 1000,
    width: gbData.sysWidth,
    height: gbData.sysHeight
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  },
  showStory: function (event) {
    var index = event.currentTarget.dataset.index
    switch (Number(index) + 1) {
      case 1:
        wx.navigateTo({
          url: 'pages/story/story1/story1',
        });
        break;
      case 2:
        wx.navigateTo({
          url: 'pages/story/story2/story2',
        });
        break;
      case 3:
        wx.navigateTo({
          url: 'pages/story/story3/story3',
        });
        break;
      case 4:
        wx.navigateTo({
          url: 'pages/story/story4/story4',
        });
        break;
      case 5:
        wx.navigateTo({
          url: 'pages/story/story5/story5',
        });
        break;
    }
  }
})